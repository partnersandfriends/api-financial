const mongoose = require('mongoose');

mongoose.set('useCreateIndex', true);
const ExpenseSchema = new mongoose.Schema({
  idUser: { type: String, required: true },
  companyName: { type: String, required: true },
  value: { type: Number, required: true },
  details: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Detail',
  },
});

const ExpenseModel = mongoose.model('Expense', ExpenseSchema);
module.exports = ExpenseModel;
