const mongoose = require('mongoose');

mongoose.set('useCreateIndex', true);
const DetailSchema = new mongoose.Schema({
  cnpj: { type: String, required: true },
  cardNumber: { type: String, required: true },
  timeStamp: { type: Number, default: Date.now(), required: true },
  mapLocation: { type: String, required: true },
});

const DetailModel = mongoose.model('Detail', DetailSchema);
module.exports = DetailModel;
