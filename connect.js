const mongoose = require('mongoose');

const db = async () => {
  const options = {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  };
  process.stdout.write(`DB_HOST => ${process.env.DB_HOST}`);
  await mongoose.connect(`mongodb://${process.env.DB_HOST}/financial`, options);
  mongoose.connection.on('connected', () => process.stdout('Connected'));
  mongoose.connection.on('error', err => new Error(`MongoDB error: ${err}`));
};

process.on('SIGINT', () => {
  mongoose.connection.close(() => {
    process.stdout.write('Mongoose disconnected through app termination');
    process.exit(0);
  });
});

module.exports = db;
