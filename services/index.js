const ExpenseModel = require('../models/expense');
const DetailModel = require('../models/detail');

const Detail = {
  findById: async _id => {
    try {
      return await DetailModel.findById(_id);
    } catch (error) {
      throw new Error(error);
    }
  },
  save: async details => {
    try {
      const newDetail = new DetailModel(details);
      const { id } = await newDetail.save();
      return id;
    } catch (err) {
      throw new Error(err);
    }
  },
};

const Expense = {
  find: async idUser => {
    try {
      const user = await ExpenseModel.find({ idUser });
      return user;
    } catch (error) {
      throw new Error(error);
    }
  },
  save: async expense => {
    try {
      const { idUser, companyName, value, username, details } = expense;
      const id = await Detail.save(details);
      const newExpense = new ExpenseModel({
        idUser,
        companyName,
        value,
        username,
        details: id,
      });
      await newExpense.save();
      return 'success';
    } catch (err) {
      throw new Error(err);
    }
  },
};
const services = { Expense, Detail };
module.exports = services;
