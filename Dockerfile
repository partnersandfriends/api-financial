FROM node:13.7.0-alpine3.10

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install

COPY . .
EXPOSE 4011
ENV PORT=4011
ENV DB_HOST=local-mongodb:27017
CMD ["node", "index.js"]