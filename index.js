const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const services = require('./services');
const db = require('./connect');

db();

const app = express();
app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.get('/', (req, res) => {
  res.send('Server is working ');
});

app.get('/expense/:idUser', async (req, res) => {
  const { idUser } = req.params;
  const expenses = await services.Expense.find(idUser);
  return res.json(expenses);
});

app.get('/expense/:idUser/detail/:_id', async (req, res) => {
  const { _id } = req.params;
  const details = await services.Detail.findById(_id);
  return res.json(details);
});

app.post('/expense', async (req, res) => {
  const result = await services.Expense.save(req.body);
  res.send(result);
});
process.stdout.write(`PORT => ${process.env.PORT}`);
app.listen(process.env.PORT, () => {
  process.stdout.write('Server runing');
});
