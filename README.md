# API Rest

Você está na versão 2 do Projeto de exemplo API REST, não se esqueça de olhar a lista de TAGs para acompanhar a evolução do projeto

## Cenário 1: Usando o docker como recurso externo de banco de dados

### Opção A: Baixando e executando a imagem de banco de dados

```bash
# Baixando a imagem
docker pull mongo:3.4

# Criando o container
docker container create -p 27017:27017 --name local-mongodb -d mongo:3.4

# Iniciando o container
docker container start local-mongodb
```

### Opção B: Executando as 3 operações acima em 1 só comando

```bash
# Baixando a imagem, criando o container e iniciando
docker run -p 27017:27017 --name local-mongodb -d mongo:3.4
```

# Criando variavél de ambiente para acessar o banco de dados

```bash
export DB_HOST=localhost:27017
```

# Executando a aplicação

```bash
npm run start
```

# Finalizando o cenário 1

Pare a aplicação e remova o container local-mongodb

```bash
docker container rm local-mongodb -f
```

## Cenário 2: Usando o docker na aplicação

Gerando a imagem do projeto

```bash
docker build -t {user}/api-financial:{version} .
```

### Opção A: Criando e inicializando os containers permitindo acesso entre eles

```bash
# Baixando a imagem, criando o container e iniciando
docker run -p 27017:27017 --name local-mongodb -d mongo:3.4
```

```bash
docker run -d --link local-mongodb --name api-rest -p 4011:4011 --env DB_HOST=local-mongodb:27017 leviditomazzo/api-financial:v1
```

#### Finanlizando opção A

Removendo os containers

```bash
docker container rm api-rest local-mongodb -f
```

### Opção B: Executando a aplicação com docker-compose

```bash
docker-compose up -d
```

Removendo os containers criados

```bash
docker-compose down
```

## Docker Hub

Efetivando o login no Docker Hub

```bash
docker login
```

Enviando a imagem para o Docker Hub

```bash
docker push {user}/api-financial:{version}
```

Baixando a imagem do Docker Hub

Docker Pull Command

```bash
docker pull {user}/api-financial:{version}
```
